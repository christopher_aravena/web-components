# Web Components
Es un conjunto de diferentes tecnologías permitiendo crear elementos personalizados reusables, con funcionalidades apartadas del resto del codigo.

Web Components consiste en tres principales tecnologías, cuales pueden ser usadas en conjunto para crear versatiles elementos personalizados 
con su funcionalidad encapsulada.

----

## Tecnologías

* Custom elements
* Shadow DOM
* HTML Templates

### Custom Elements 
Api de Javascript que permite definir elementos personalizados y su comportamiento.

* Autonomous Custom Elements
* Customized Buil-it Elements

### Shadow DOM
Api de Javascript que permite adjuntar un "shadow" DOM a un elemento, que se procesa por separado del DOM del documento principal.

### HTML Template
Elementos que te permiten escribir markup templates, la idea de estos es mantener el html que no se procesa inmediatamente cuando se carga una página, pero se puede crear una instancia posteriormente durante el tiempo de ejecución utilizando JavaScript.

----

##  Lifecycle Callback
* connectedCallback()  **Custom element added to the page** 
* disconnectedCallback() **Custom element removed from page** 
* adoptedCallback() **Custom element moved to new page** 
* attributeChangedCallback() **Custom element attributes changed** 