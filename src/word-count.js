class WordCount extends HTMLParagraphElement {
  constructor() {
      super();
      /**
       * cuenta las palabras del elemento padre
       */
      const wcParent = this.parentNode;
      
      function countWords(node) {
          const text = node.innerText || node.textContent;
          return text.split(/\s+/g).length;
      }

      const count = `Words: ${countWords(wcParent)}`;

      /**
       * Crea un shadowRoot
       */
      const shadow = this.attachShadow({
          mode: 'open'
      });

      /**
       * Crea un texto con la cantidad de palabras
       */
      const text = document.createElement('span');
      text.textContent = count;

      /**
       * Se añade al shadow root
       */
      shadow.appendChild(text);

      // Actualiza contador cuando el contenido cambia
      setInterval(function() {
          const count = `Words: ${countWords(wcParent)}`;
          text.textContent = count;
      }, 200);
  }
}

/**
 * name: DOMString que representa el nombre del elemento personalizado.
 * constructor: Un objeto de clase que define el comportamiento del elemento.
 * options: Especifica el elemento del cual se hereda.
 * customeElements: Intancia de CustomElementRegistry, interfaz que provee metodos
 * para registrar los elementos personalizados.
 */
customElements.define('word-count', WordCount, {
  extends: 'p'
});