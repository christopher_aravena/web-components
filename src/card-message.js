const template = document.createElement('template');

template.innerHTML = `
  <style>
    .card-content {
      box-shadow: 0 4px 8px rgba(0,0,0,0.2);
      transition: 0.3s;
      border-radius: 5px;
      padding: 20px;
      margin: 10px;
      background-color:#E8EFEF;
    }
    .card-content:hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
  </style>
  <div class="card-content"></div>
`;


function updateContent(elem) {
  const shadow = elem.shadowRoot;
  shadow.querySelector('h3').textContent = elem.getAttribute('title');
  shadow.querySelector('p').textContent = elem.getAttribute('content');
}

/**
 * updateStyle toma un elemento, obtiene su shadow root, encuentra su 
 * elemento <style> y cambia background-color al 
 * estilo.
 */
function updateStyle(elem) {
  const shadow = elem.shadowRoot;
  shadow.querySelector('style').textContent = `
    .card-content { 
      box-shadow: 0 4px 8px rgba(0,0,0,0.2);
      transition: 0.3s;
      border-radius: 5px;
      padding: 20px;
      margin: 10px;
      background-color: ${elem.getAttribute('background')};
    }
    .card-content:hover {
      box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
  `
}

class Message extends HTMLElement {
  /**
   * Se especifica los atributos observados para
   * que attributeChangedCallback funcione
   */
  static get observedAttributes(){
    return ['title','content','background'];
  }
  /**
   * Atachamos un shadow DOM al elemento, entonces
   * añadimos un clone del template al shadow root.
   * 
   */
  constructor() {
    /**
     * Lo que hacemos primero en el constructor es llamar
     * a super(). De esta manera se establece la correcta 
     * cadena de prototipo.
     */
    super();
    /**
     * Attachamos el shadowDOM al elemento. 
     * Shadow DOM se refiere a la capacidad del navegador
     * para incluir un subárbol de elementos DOM en la re_
     * presentación de un documento, pero no en el árbol DOM
     * del documento principal.
     */
    const shadow = this.attachShadow({mode:'open'});
    shadow.appendChild(template.content.cloneNode(true));
    /**
     * Hacemos a card-content nuestro nodo raiz, luego 
     * añadimos los elementos <h3> y <p> vacios a nuestro
     * nodo raiz.
     */
    this.rootNode = this.shadowRoot.querySelector('.card-content');
    this.rootNode.appendChild(document.createElement('h3'));
    this.rootNode.appendChild(document.createElement('p'));
  }

  /* Los updates son todas manejadas por el life cycle callbacks. */

  /**
   * Se invoca cada vez que el elemento personalizado se agrega
   * a un elemento conectado a un documento.
   */
  connectedCallback() {
    console.log('Custom card element added to the page');
    updateContent(this);
  }
  /**
   * Se invoca cada vez que el elemento personalizado se desconecta
   * del DOM del documento.
   */
  disconnectedCallback() {
    console.log('Custom card element removed from page.');
  }
  /**
   * Se invoca cada vez que el elemento personalizado es movido a un 
   * nuevo documento.
   */
  adoptedCallback() {
    console.log('Custome card element moved to new page.');
  }
  /**
   * Se invoca cada vez que se agrega, elimina o cambia uno de los atributos
   * de los elementos personalizados. Los atributos para los que se notará el
   * cambio se especifican en un metodo static get observedAttributes()
   */
  attributeChangedCallback(name, oldValue, newValue) {
    console.log('Custome card element attributes changed');
    updateStyle(this);
    updateContent(this);
  }
}
/**
 * name: DOMString que representa el nombre del elemento personalizado.
 * constructor: Un objeto de clase que define el comportamiento del elemento.
 * customeElements: Intancia de CustomElementRegistry, interfaz que provee metodos
 * para registrar los elementos personalizados.
 */
customElements.define('card-message', Message);


const add = document.querySelector('.add');
const update = document.querySelector('.update');
const remove = document.querySelector('.remove');
let card;

update.disabled = true;
remove.disabled = true;

function random(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

add.onclick = function() {
  card = document.createElement('card-message');
  card.setAttribute('title', 'Error Message');
  card.setAttribute('content', 'Hi, all is wrong');
  document.body.appendChild(card);
  update.disabled = false;
  remove.disabled = false;
  add.disabled = true;
}
update.onclick = function() {
  card.setAttribute('title', 'New Error Message');
  card.setAttribute('content', 'Hi, this is a new error message');
  card.setAttribute('background', `rgb(${random(0, 255)}, ${random(0, 255)}, ${random(0, 255)})`);
}
remove.onclick = function() {
  document.body.removeChild(card);
  update.disabled = true;
  remove.disabled = true;
  add.disabled = false;
} 